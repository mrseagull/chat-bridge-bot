<?php

namespace antonchaikin\ChatBridgeBot;

use MongoDB\Client;
use MongoDB\Collection;


class MognoDB
{

    private static $client;
    private static $database;

    public static function connect()
    {


        $params = [
            "username"         => ChatBotConfig::$MONGODB_USERNAME,
            "authSource"       => ChatBotConfig::$MONGODB_AUTHSOURCE,
            "password"         => ChatBotConfig::$MONGODB_PASSWORD,
            "connectTimeoutMS" => 360000,
            "socketTimeoutMS"  => 360000,
            'poolSize'         => 1000,
            'ssl' => false,
        ];


        $server = 'mongodb://' . ChatBotConfig::$MONGODB_HOST . ":" . ChatBotConfig::$MONGODB_PORT;


        self::$client = new Client($server, $params);

        $db    =  ChatBotConfig::$MONGODB_DATABASE;
        self::$database = self::$client->$db;
    }



    public static function id($val)
    {

        if ($val && gettype($val) == 'string') {
            return self::getId($val);
        } else {
            return $val;
        }
    }

    public static function getId($id = false)
    {
        if ($id) {
            $id = (string) $id;

            if (preg_match('/^[0-9a-f]{24}$/i', $id) === 1) {
                return $id ? new \MongoDB\BSON\ObjectID($id) : new \MongoDB\BSON\ObjectID();
            } else {
                return false;
            }
        }
    }



    public static function collection($collection): Collection
    {
        if (!self::$database) {
            self::connect();
        }

        return self::$database->$collection;
    }
}
