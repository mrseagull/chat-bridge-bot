<?php

namespace antonchaikin\ChatBridgeBot\Types;

use antonchaikin\ChatBridgeBot\BaseChatBot;
use antonchaikin\ChatBridgeBot\Translator;


/**
 * Класс State представляет состояние бота.
 */
class State
{

    /**
     * Конструктор класса State.
     *
     */

    private BaseChatBot $root;

    public function __construct($root)
    {
        $this->root = $root;
    }

    /**
     * Returns the bot's state key (static method).
     *
     * @return string Bot state key.
     */
    public static function key()
    {
        return get_called_class();
    }

    public function save(string $key, mixed $val)
    {
        $this->root->setChatData($key, $val);
    }

    public function load(string $key)
    {

        return $this->root->getChatData($key);
    }

    public function getFile()
    {

        return $this->root->getFile();
    }

    public function goTo(string $stateKey)
    {
        $this->root->goToState($stateKey);
    }

    public function message(): Message
    {
        return new Message($this->root);
    }


    public function getPressedButton()
    {
        $buttons = $this->root->currentChat->buttons ?? [];

        if ($this->root->currentChat->source == "telegram") {
            $msg = $this->root->getPressedButton();


            if ($buttons && count($buttons) > 0) {

                foreach ($buttons as $key => $val) {
                    $buttons[$key] = Translator::Translate($val, $this->root->getClientLanguage());
                }
            }

            $key = null;
            foreach ($buttons as $keyVal => $val) {
                if (md5($val) == md5($msg)) {
                    $key = $keyVal;
                }
            }
            return $key;
        }

        if ($this->root->currentChat->source == "viber") {
            $key = $this->root->getPressedButton();


            $buttons = array_keys($buttons ?? []);

            $buttons[] = "next";

            if (in_array($key, $buttons)) {
                return $key;
            } else {
                return null;
            }
        }
    }

    public function getMessageText()
    {

        if (!$this->getPressedButton()) {

            return $this->root->getMessageText();
        } else {
            return null;
        }
    }


    public function onStateEntry()
    {
    }



    public function inlineResolve(mixed $params)
    {
    }


    public function resolve(string | null $msg, string | null $pressedButton)
    {
    }
}
