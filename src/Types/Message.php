<?php

namespace antonchaikin\ChatBridgeBot\Types;

use antonchaikin\ChatBridgeBot\BaseChatBot;
use antonchaikin\ChatBridgeBot\Translator;


final class Message
{


    private $text = null;
    private $buttons = null;
    private $inline = null;

    private BaseChatBot $root;

    public function __construct($root)
    {
        $this->root = $root;
    }


    public function text(string $text): self
    {
        $this->text = $text;
        return $this;
    }


    public function buttons(array $buttons): self
    {
        $this->buttons = $buttons;
        return $this;
    }


    public function inline(array $inline): self
    {
        $this->inline = $inline;
        return $this;
    }

    public function send()
    {
        $this->root->sendToChat($this->text, $this->buttons, $this->inline);
    }
}
