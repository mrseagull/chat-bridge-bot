<?php

declare(strict_types=1);

namespace antonchaikin\ChatBridgeBot\Enums;


enum Language: string
{
    case Afrikaans = 'af';
    case Albanian = 'sq';
    case Amharic = 'am';
    case Arabic = 'ar';
    case Armenian = 'hy';
    case Azerbaijani = 'az';
    case Bengali = 'bn';
    case Bosnian = 'bs';
    case Bulgarian = 'bg';
    case CanadianFrench = 'fr-CA';
    case Catalan = 'ca';
    case Chinese = 'zh';
    case ChineseTraditional = 'zh-TW';
    case Croatian = 'hr';
    case Czech = 'cs';
    case Danish = 'da';
    case Dari = 'fa-AF';
    case Dutch = 'nl';
    case English = 'en';
    case Estonian = 'et';
    case Finnish = 'fi';
    case French = 'fr';
    case Georgian = 'ka';
    case German = 'de';
    case Greek = 'el';
    case Gujarati = 'gu';
    case HaitianCreole = 'ht';
    case Hausa = 'ha';
    case Hebrew = 'he';
    case Hindi = 'hi';
    case Hungarian = 'hu';
    case Icelandic = 'is';
    case Indonesian = 'id';
    case Irish = 'ga';
    case Italian = 'it';
    case Japanese = 'ja';
    case Kannada = 'kn';
    case Kazakh = 'kk';
    case Korean = 'ko';
    case Latvian = 'lv';
    case Lithuanian = 'lt';
    case Macedonian = 'mk';
    case Malay = 'ms';
    case Malayalam = 'ml';
    case Maltese = 'mt';
    case Marathi = 'mr';
    case MexicanSpanish = 'es-MX';
    case Mongolian = 'mn';
    case Norwegian = 'no';
    case Pashto = 'ps';
    case Persian = 'fa';
    case Polish = 'pl';
    case PortugalPortuguese = 'pt-PT';
    case Portuguese = 'pt';
    case Punjabi = 'pa';
    case Romanian = 'ro';
    case Russian = 'ru';
    case Serbian = 'sr';
    case Sinhala = 'si';
    case Slovak = 'sk';
    case Slovenian = 'sl';
    case Somali = 'so';
    case Spanish = 'es';
    case Swahili = 'sw';
    case Swedish = 'sv';
    case Tagalog = 'tl';
    case Tamil = 'ta';
    case Telugu = 'te';
    case Thai = 'th';
    case Turkish = 'tr';
    case Ukrainian = 'uk';
    case Urdu = 'ur';
    case Uzbek = 'uz';
    case Vietnamese = 'vi';
    case Welsh = 'cy';

    public static function exists(string $value): bool
    {
        foreach (self::cases() as $case) {
            if ($case->value === $value) {
                return true;
            }
        }
        return false;
    }
}
