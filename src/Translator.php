<?php

namespace antonchaikin\ChatBridgeBot;

use antonchaikin\ChatBridgeBot\Enums\Language;
use Aws\Exception\AwsException;




final class Translator
{

    public static function Translate($text, string $to)
    {

        if (!ChatBotConfig::$useTranslate) return $text;

        $from = ChatBotConfig::$originalBotLanguage;

        if (!Language::exists($to)) {
            return $text;
        }

        if ($from->value == $to) {
            return $text;
        }

        $hash = md5($text);

        $item = MognoDB::collection("_localization")->findOne([
            "hash" => $hash,
        ]);

        if (!$item) {
            MognoDB::collection("_localization")->insertOne([
                "hash" => $hash,
                "ru" => $text,
            ]);

            $item = MognoDB::collection("_localization")->findOne([
                "hash" => $hash,
            ]);
        }

        if (isset($item[$to])) {
            return $item[$to];
        } else {

            $string = self::AWSTranslation($text, $from->value, $to);

            MognoDB::collection("_localization")->updateOne([
                "hash" => $hash,
            ], [
                '$set' => [
                    $to => $string,

                ],
            ]);
            return $string;
        }
    }


    private static function AWSTranslation(string $text, string $currentLanguage, string $targetLanguage)
    {

        try {
            $client = new \Aws\Translate\TranslateClient(ChatBotConfig::$awsTranslateConfig);

            $result = $client->translateText([
                'SourceLanguageCode' => $currentLanguage,
                'TargetLanguageCode' => $targetLanguage,
                'Text' => $text,
            ]);
            return $result->get('TranslatedText');
        } catch (AwsException $e) {
            return $text;
        }
    }
}
