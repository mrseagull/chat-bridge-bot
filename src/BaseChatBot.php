<?php

namespace antonchaikin\ChatBridgeBot;

use antonchaikin\ChatBridgeBot\Types\State;

final class BaseChatBot
{



    public $currentChat = null;

    private $startStateKey = null;

    public $payload = null;

    public function __construct(string $startStateKey)
    {
        $this->startStateKey = $startStateKey;
        return $this;
    }




    public function getBotConfig()
    {

        $botConfig = MognoDB::collection("bots_config")->findOne([
            "bot" =>  ChatBotConfig::$botName,
        ]);

        if (!$botConfig) {
            MognoDB::collection("bots_config")->insertOne([
                "bot" => ChatBotConfig::$botName,
            ]);

            $botConfig = MognoDB::collection("bots_config")->findOne([
                "bot" => ChatBotConfig::$botName,
            ]);
        }
        return $botConfig;
    }

    public function initWebHook()
    {
        if (ChatBotConfig::$webHookUrl && ChatBotConfig::$botName) {

            $botConfig = $this->getBotConfig();

            if (ChatBotConfig::$viberBotToket && (!isset($botConfig->viberWebHook) || $botConfig->viberWebHook != ChatBotConfig::$webHookUrl)) {

                Utils::sendViberRequest("set_webhook", [
                    'url' => ChatBotConfig::$webHookUrl,
                    "event_types" => [
                        "message",
                    ],
                ]);

                MognoDB::collection("bots_config")->updateOne([
                    "_id" => $botConfig->_id,
                ], [
                    '$set' => [

                        "viberWebHook" => ChatBotConfig::$webHookUrl,
                    ],
                ]);
            }

            if (ChatBotConfig::$telegramBotToken && (!isset($botConfig->telegramWebHook) || $botConfig->telegramWebHook != ChatBotConfig::$webHookUrl)) {

                $currentUrl = ChatBotConfig::$webHookUrl;
                Utils::sendTelegramRequest('setWebhook', [
                    "url" => $currentUrl,
                ]);

                MognoDB::collection("bots_config")->updateOne([
                    "_id" => $botConfig->_id,
                ], [
                    '$set' => [
                        "telegramWebHook" => ChatBotConfig::$webHookUrl,
                    ],
                ]);
            }
        }
    }

    public function getClientLanguage()
    {

        $data = $this->payload;

        $lang = "en";
        if (isset($data['message']['from']['language_code'])) {
            $lang = $data['message']['from']['language_code'];
        }

        if (isset($data['callback_query']['from']['language_code'])) {
            $lang = $data['callback_query']['from']['language_code'];
        }

        if (isset($data['sender']['language'])) {
            $lang = $data['sender']['language'];
        }

        $languageCode = substr($lang, 0, 2);

        return $languageCode;
    }

    public function getFile()
    {

        $data = $this->payload;

        if ($this->currentChat->source == "telegram") {

            $type = null;
            if (isset($data['message']['document'])) {
                $type = "document";
            }

            if (isset($data['message']['photo'])) {
                $type = "photo";
            }

            if (isset($data['message']['video'])) {
                $type = "video";
            }

            switch ($type) {
                case 'photo':
                    $file_id = $data['message'][$type][count($data['message'][$type]) - 1]['file_id'];
                    break;
                case 'document':
                case 'video':
                    $file_id = $data['message'][$type]['file_id'];
                    break;
            }

            if (isset($file_id)) {
                $file_info = file_get_contents("https://api.telegram.org/bot" . ChatBotConfig::$telegramBotToken . "/getFile?file_id=" . $file_id);
                $file_path = json_decode($file_info, true)['result']['file_path'];
                $file_url = "https://api.telegram.org/file/bot" . ChatBotConfig::$telegramBotToken . "/" . $file_path;
            }
        }

        if ($this->currentChat->source == "viber") {

            // Для Viber мы просто получаем URL медиафайла
            if (isset($data['media'])) {
                $file_url = $data['media'];
            }
        }

        return isset($file_url) ? $file_url : null;
    }

    public function getChatId($requestData)
    {

        // Получение ID чата для каждого источника
        if (isset($requestData['message']['chat']['id'])) {
            return $requestData['message']['chat']['id']; // Telegram
        } elseif (isset($requestData['sender']['id'])) {
            return $requestData['sender']['id']; // Viber
        } elseif (isset($requestData['callback_query']['message']['chat']['id'])) {
            return $requestData['callback_query']['message']['chat']['id']; // Telegram (нажатие на кнопку)
        } elseif (isset($requestData['event']) && $requestData['event'] === 'message' && isset($requestData['sender']['id'])) {
            return $requestData['sender']['id']; // Viber (нажатие на кнопку)
        }

        // Если ID чата не найден, вернуть пустое значение или выбросить исключение
        return '';
    }

    public function getWebhookSource($requestData)
    {

        // Проверка наличия полей, характерных для каждого источника
        if (isset($requestData['message']['chat']['id'])) {
            return 'telegram';
        } elseif (isset($requestData['sender']['id'])) {
            return 'viber';
        } elseif (isset($requestData['callback_query']['message']['chat']['id'])) {
            return 'telegram';
        } elseif (isset($requestData['event']) && $requestData['event'] === 'message' && isset($requestData['sender']['id'])) {
            return 'viber';
        }

        // Если источник не определен, вернуть пустое значение или выбросить исключение
        return null;
    }



    public function getPressedButton()
    {

        // Попытка декодирования JSON тела запроса
        $requestData = $this->payload;

        if ($this->currentChat->source == "telegram") {

            return $this->getMessageText();
        }

        if ($this->currentChat->source == "viber") {

            if (isset($requestData['event']) && $requestData['event'] === 'message' && isset($requestData['message']['text'])) {
                return $requestData['message']['text']; // Viber (нажатие на кнопку)
            }
        }
        // Если нажатая кнопка не найдена, вернуть пустое значение или выбросить исключение
        return '';
    }
    public function getBotChat($payload)
    {

        $chat_id = $this->getChatId($payload);
        if ($chat_id) {
            $source = $this->getWebhookSource($payload);

            $bot_chat = MognoDB::collection("bot_chats")->findOne([
                "chat_id" => $chat_id,
                "bot" => ChatBotConfig::$botName,
                "source" => $source,
            ]);

            if (!$bot_chat) {

                MognoDB::collection("bot_chats")->insertOne([
                    "bot" => ChatBotConfig::$botName,
                    "chat_id" => $chat_id,
                    "source" => $source,
                ]);

                $bot_chat = MognoDB::collection("bot_chats")->findOne([
                    "bot" => ChatBotConfig::$botName,
                    "chat_id" => $chat_id,
                    "source" => $source,
                ]);
            }

            return $bot_chat;
        } else {
            return null;
        }
    }

    public function setChatData($key, $val)
    {

        MognoDB::collection("bot_chats")->updateOne([
            "_id" => $this->currentChat->_id,
        ], [
            '$set' => [
                'data.' . $key => $val,
            ],
        ]);
    }

    public function getChatData($key)
    {

        $tmp = MognoDB::collection("bot_chats")->findOne([
            "_id" => $this->currentChat->_id,
        ]);

        return $tmp->data[$key] ?? null;
    }

    public function onState()
    {

        $key = $this->currentChat->currentStage;

        $class = $key;
        $object = new $class($this);


        $callback_params = $this->getInlineKeyboardData();

        if ($callback_params) {
            $object->inlineResolve($callback_params);
        } else {

            $object->resolve($object->getMessageText(), $object->getPressedButton());
        }
    }

    public function getInlineKeyboardData()
    {


        $requestData = $this->payload;
        $callback_query = null;


        if (isset($requestData['callback_query']['data'])) {
            $callback_query = $requestData['callback_query']; // Telegram
        }



        if ($callback_query) {

            $inlineButtonData =  MognoDB::collection("_inlineButtons")->aggregate([
                [
                    '$match' => [
                        'chat' => $this->currentChat->_id,
                        'buttons.callback_data' => $callback_query['data']
                    ]
                ],
                [
                    '$unwind' =>
                    [
                        'path' => '$buttons'
                    ]
                ],
                [
                    '$match' =>
                    [
                        'buttons.callback_data' => $callback_query['data']
                    ]
                ],
                [
                    '$project' =>
                    [
                        'text' => '$buttons.text',
                        'params' => '$buttons.params'
                    ]
                ]
            ])->toArray()[0] ?? null;

            if ($inlineButtonData) {

                Utils::sendTelegramRequest("answerCallbackQuery", [
                    'callback_query_id' => $callback_query['id'],
                    //  'text' => "✅ " . $inlineButtonData->text,
                    //   'show_alert' => false
                ]);


                return $inlineButtonData->params;
            } else {

                Utils::sendTelegramRequest("answerCallbackQuery", [
                    'callback_query_id' => $callback_query['id'],
                    'text' => "⛔️",
                    'show_alert' => false
                ]);
            }
        }

        return null;
    }

    public function sendToChat($text, $buttons = null, $inline = null)
    {




        $text = Translator::Translate($text,  $this->getClientLanguage());
        if ($buttons && count($buttons) > 0) {

            foreach ($buttons as $key => $val) {
                $buttons[$key] = Translator::Translate($val, $this->getClientLanguage());
            }
        }


        if ($inline && count($inline) > 0) {

            $inline_tmp = [];
            foreach ($inline as $name => $params) {

                $key = Translator::Translate($name, $this->getClientLanguage());
                $inline_tmp[$key] = $params;
            }
            $inline = $inline_tmp;
        }

        MognoDB::collection("bot_chats")->updateOne([
            "_id" => $this->currentChat->_id,
        ], [
            '$set' => [
                "buttons" => $buttons
            ],
        ]);


        if ($this->currentChat->source == "viber") {

            $params = [
                'receiver' => $this->currentChat->chat_id,
                'type' => 'text',
                'text' => $text,
            ];

            if ($buttons) {

                $params['keyboard'] = [
                    "DefaultHeight" => true,

                    'Buttons' => Utils::formatButtonsForViber($buttons),
                ];
            }

            Utils::sendViberRequest('send_message', $params);
        }

        if ($this->currentChat->source == "telegram") {




            $params = [
                'chat_id' => $this->currentChat->chat_id,
                'text' => $text,
                'parse_mode' => 'html'
            ];

            $reply_markup = [];

            if ($inline) {
                $reply_markup = [
                    ...$reply_markup,
                    'inline_keyboard' => Utils::formatInlineButtonsForTelegram($inline, $this->currentChat->_id),
                ];
            }

            if ($inline && $buttons) {

                $params['reply_markup'] = json_encode($reply_markup);

                Utils::sendTelegramRequest('sendMessage', $params);

                $reply_markup = [];
                $params = [
                    'text' => "...",
                    'chat_id' => $this->currentChat->chat_id,
                ];
            }


            if ($buttons) {

                $reply_markup = [
                    ...$reply_markup,
                    'keyboard' => Utils::formatButtonsForTelegram($buttons),
                    'resize_keyboard' => true,
                ];
            } else {

                $reply_markup = [
                    ...$reply_markup,
                    'remove_keyboard' => true,
                ];
            }

            if (count($reply_markup) > 0)
                $params['reply_markup'] = json_encode($reply_markup);

            Utils::sendTelegramRequest('sendMessage', $params);
        }
    }

    public function setState($key)
    {

        MognoDB::collection("bot_chats")->updateOne([
            "_id" => $this->currentChat->_id,
        ], [
            '$set' => [
                'currentStage' => $key,
            ],
        ]);
    }

    public function goToState($key)
    {

        MognoDB::collection("bot_chats")->updateOne([
            "_id" => $this->currentChat->_id,
        ], [
            '$push' => [
                'way' => $key,
            ],
        ]);

        $this->setState($key);

        $class = $key;
        $object = new $class($this);

        $object->onStateEntry();
    }

    public function startState()
    {

        $key = $this->startStateKey;

        if ($key) {

            $this->setState($key);
            $class = $key;
            $object = new $class($this);

            $object->onStateEntry();
        }
    }

    public function sendPhotoToChat($photoUrl, $caption = '')
    {

        if ($this->currentChat->source == "viber") {

            $params = [
                'receiver' => $this->currentChat->chat_id,
                'type' => 'picture',
                'media' => $photoUrl,
            ];

            Utils::sendViberRequest('send_message', $params);
        }

        if ($this->currentChat->source == "telegram") {

            $params = [
                'chat_id' => $this->currentChat->chat_id,
                'photo' => $photoUrl,
                'caption' => $caption,
            ];

            Utils::sendTelegramRequest('sendPhoto', $params);
        }
    }

    function getMessageText()
    {

        // Попытка декодирования JSON тела запроса
        $requestData = $this->payload;

        // Получение текста сообщения для каждого источника
        if (isset($requestData['message']['text'])) {
            return $requestData['message']['text']; // Telegram
        } elseif (isset($requestData['message']['message']['text'])) {
            return $requestData['message']['message']['text']; // Viber
        }

        // Если текст сообщения не найден, вернуть пустое значение или выбросить исключение
        return '';
    }

    function extractPhoneFromWebhook()
    {
        $phone = null;

        $webhookData = $this->payload;

        // Проверяем, является ли входящий запрос от Viber бота
        if (isset($webhookData['event']) && $webhookData['event'] === 'conversation_started') {
            if (isset($webhookData['user']['name'])) {
                // Здесь можно добавить код для обработки имени пользователя, если необходимо
            }
            if (isset($webhookData['user']['id']['phone_number'])) {
                $phone = $webhookData['user']['id']['phone_number'];
            }
        }

        // Проверяем, является ли входящий запрос от Telegram бота
        if (isset($webhookData['message']['contact'])) {
            if (isset($webhookData['message']['contact']['phone_number'])) {
                $phone = $webhookData['message']['contact']['phone_number'];
            }
        }
        if ($phone) {
            $this->setChatData("phone", $phone);
        }
    }

    public function start()
    {

        header("HTTP/1.1 200 OK");

        $this->initWebHook();

        $payloadData = file_get_contents('php://input');
        $this->payload = json_decode($payloadData, true);

        $source = $this->getWebhookSource($this->payload);

        $hash = md5($payloadData);

        $duble = MognoDB::collection("bot_messages")->findOne([
            "hash" => $hash,
        ]);

        if ($duble) {
            Utils::response([
                "success" => true,
            ]);
        }

        MognoDB::collection("bot_messages")->insertOne([
            "source" => $source,
            "bot" => ChatBotConfig::$botName,
            "data" => $this->payload,
            "hash" => $hash,
        ]);

        if ($source) {

            $this->currentChat = $this->getBotChat($this->payload);

            if ($this->currentChat) {
                $this->extractPhoneFromWebhook();

                if ($this->getMessageText() != "/start" && isset($this->currentChat->currentStage) && $this->currentChat->currentStage) {

                    $this->onState();
                } else {
                    $this->startState();
                }
            }
        }

        Utils::response([
            "success" => true,
        ]);
    }
}
