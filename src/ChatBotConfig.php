<?php

namespace antonchaikin\ChatBridgeBot;

use antonchaikin\ChatBridgeBot\Enums\Language;

class ChatBotConfig
{

    public static $botName = null;

    public static  $telegramBotToken = null;
    public static $viberBotToket = null;
    public static $webHookUrl = null;


    public static $MONGODB_USERNAME = null;
    public static $MONGODB_AUTHSOURCE = "admin";
    public static $MONGODB_PASSWORD = null;
    public static $MONGODB_HOST = 'localhost';
    public static $MONGODB_PORT = 27017;
    public static  $MONGODB_DATABASE = "ChatBot";

    public static Language $originalBotLanguage = Language::Russian;


    public static bool $useTranslate = false;
    public static $awsTranslateConfig = [

        'version'     => 'latest',
        'region' => 'us-west-2',
        'credentials' => [
            "key" => "credentials_key",
            "secret" => "credentials_secret"
        ]

    ];
}
