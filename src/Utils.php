<?php

namespace antonchaikin\ChatBridgeBot;



final class Utils
{


    public static function sentByChatId(mixed $chatId, string $msg)
    {

        $chat = MognoDB::collection("bot_chats")->findOne([
            "_id" => MognoDB::id($chatId)
        ]);
        if (!$chat) return;

        if ($chat->source == "viber") {

            $params = [
                'receiver' =>  $chat->chat_id,
                'type' => 'text',
                'text' => $msg,
            ];

            Utils::sendViberRequest('send_message', $params);
        }

        if ($chat->source == "telegram") {

            $params = [
                'chat_id' => $chat->chat_id,
                'text' => $msg,
                'parse_mode' => 'html'
            ];

            Utils::sendTelegramRequest('sendMessage', $params);
        }
    }

    public static function response($data)
    {
        header("HTTP/1.1 200 OK");
        echo json_encode($data);
        exit;
    }

    // Function for sending POST requests to Viber API
    public static function  sendViberRequest($method, $params = [])
    {
        $url = 'https://chatapi.viber.com/pa/' . $method;

        $params['min_api_version'] = 7;
        $headers = [
            'X-Viber-Auth-Token: ' . ChatBotConfig::$viberBotToket,
            'Content-Type: application/json',
        ];

        MognoDB::collection("bot_requests")->insertOne([
            "bot" => ChatBotConfig::$botName,
            "headers" => $headers,
            "params" => $params,
            "source" => "viber",
        ]);

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }


    // Function for sending POST requests to Telegram API
    public static function sendTelegramRequest($method, $params = [])
    {

        MognoDB::collection("bot_requests")->insertOne([
            "bot" => ChatBotConfig::$botName,
            "params" => $params,
            "source" => "telegram",
        ]);

        $url = 'https://api.telegram.org/bot' .  ChatBotConfig::$telegramBotToken . '/' . $method;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }






    public static function formatInlineButtonsForTelegram($buttons, $currentChat)
    {
        $formattedButtons = [];
        $inlineToData = [];

        foreach ($buttons as $name => $params) {

            $callback_data = bin2hex(random_bytes(12));
            $button = [
                'text' =>  $name,
                'callback_data' => $callback_data
            ];


            $inlineToData[] = [
                ...$button,
                'params' => $params
            ];

            $formattedButtons[] = $button;
        }

        if (count($inlineToData) > 0) {
            MognoDB::collection("_inlineButtons")->insertOne([
                "buttons" => $inlineToData,
                "chat" => $currentChat
            ]);
        }

        $formattedButtons = array_chunk($formattedButtons, 2);


        return $formattedButtons;
    }



    public static function formatButtonsForTelegram($buttons)
    {
        $formattedButtons = [];

        foreach ($buttons as $actionBody => $text) {
            $button = [
                'text' => $text,
                'callback_data' => $actionBody,
            ];

            if ($actionBody === 'location') {
                $button['request_location'] = true;
            } elseif ($actionBody === 'contact') {
                $button['request_contact'] = true;
            }

            $formattedButtons[] = $button;
        }

        $formattedButtons = array_chunk($formattedButtons, 2);

        return $formattedButtons;
    }


    public static function formatButtonsForViber($buttons)
    {

        $formattedButtons = [];

        foreach ($buttons as $actionBody => $text) {
            $button = [
                'Text' => $text,
                'ActionBody' => $actionBody,
                'Columns' => 6,
                'Rows' => 1,
                'ActionType' => 'reply',
                'BgColor' => '#F5F5F5',
                'TextSize' => 'large',
                'TextHAlign' => 'center',
                'TextVAlign' => 'middle',

            ];

            if ($actionBody === 'location') {
                $button['ActionType'] = 'location-picker';
            } elseif ($actionBody === 'contact') {
                $button['ActionType'] = 'share-phone';
            }

            $formattedButtons[] = $button;
        }

        return $formattedButtons;
    }
}
